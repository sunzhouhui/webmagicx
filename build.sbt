name := "webmagicx"

version := "0.1.0"

scalaVersion := "2.12.4"


libraryDependencies ++= Seq(
  "org.apache.httpcomponents" % "httpclient" % "4.5.2",
  "com.jayway.jsonpath" % "json-path" % "2.4.0",
  "org.slf4j" % "slf4j-api" % "1.7.6",
  "org.slf4j" % "slf4j-log4j12" % "1.7.6",
  "us.codecraft" % "xsoup" % "0.3.1",
  "com.alibaba" % "fastjson" % "1.2.28",
  "log4j" % "log4j" % "1.2.17",
  "commons-collections" % "commons-collections" % "3.2.2",
  "redis.clients" % "jedis" % "2.9.0",
  "org.scalaj" % "scalaj-http_2.12" % "2.3.0",
  "com.zaxxer" % "HikariCP" % "2.7.4",
  "mysql" % "mysql-connector-java" % "5.1.37",
  "org.apache.commons" % "commons-csv" % "1.5",
  "org.quartz-scheduler" % "quartz" % "2.2.1",
  "commons-cli" % "commons-cli" % "1.4",
  "com.typesafe.play" % "play-json_2.12" % "2.6.8",
  "org.jsoup" % "jsoup" % "1.10.3",
  "org.apache.commons" % "commons-io" % "1.3.2",
  "org.scala-lang.modules" %% "scala-xml" % "1.0.6",
  "com.typesafe.akka" % "akka-http_2.12" % "10.0.11",
  "org.apache.commons" % "commons-lang3" % "3.7"
)

enablePlugins(JavaAppPackaging, SbtTwirl)

javacOptions ++= Seq("-encoding", "UTF-8")

