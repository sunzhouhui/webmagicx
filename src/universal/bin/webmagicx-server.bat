@echo off

rem webmagicx 的 windows 启动命令

setlocal enabledelayedexpansion

set "JAVA_HOME=%JAVA_HOME%"

set "CURRENT_DIR=%cd%"
cd ..
set "WEBMAGICX_HOME=%cd%"
set "CONF_DIR=%WEBMAGICX_HOME%\conf"
set "LIB_DIR=%WEBMAGICX_HOME%\lib"
set "SPIDER_CONF_DIR=%WEBMAGICX_HOME%\spiderConf"
cd %CURRENT_DIR%

set "CLASS_PATH=%CONF_DIR%"
for %%c in (%LIB_DIR%\*.jar) do @set CLASS_PATH=!CLASS_PATH!;%%c
"%JAVA_HOME%\bin\java" -classpath "%CLASS_PATH%" "org.luosl.webmagicx.Application" %1 %2 %3 %4 %5 %6 %7 %8 %9
:end