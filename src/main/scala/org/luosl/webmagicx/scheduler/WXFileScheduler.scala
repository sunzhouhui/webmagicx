package org.luosl.webmagicx.scheduler

import org.luosl.webmagicx.conf.{XmlProps, SpiderConf}
import us.codecraft.webmagic.Task
import us.codecraft.webmagic.scheduler.FileCacheQueueScheduler

import org.luosl.webmagicx.conf.PropType._
import org.luosl.webmagicx.conf.MatcherConverters._

@Deprecated
class WXFileScheduler(dataDir:String, task:Task) extends FileCacheQueueScheduler(dataDir) {
  def this(sc:SpiderConf, task:Task, props:XmlProps) = this(
    props.valueOrDefault("dataDir", "value")(strType)(""),
    task
  )
}
