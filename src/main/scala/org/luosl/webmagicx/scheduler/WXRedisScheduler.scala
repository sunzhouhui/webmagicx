package org.luosl.webmagicx.scheduler

import java.io.Closeable

import org.apache.commons.pool2.impl.GenericObjectPoolConfig
import org.luosl.webmagicx.conf.{XmlProps, SpiderConf}
import redis.clients.jedis.{JedisPool, Protocol}
import us.codecraft.webmagic.Task
import us.codecraft.webmagic.scheduler.RedisScheduler
import org.luosl.webmagicx.conf.PropType._
import org.luosl.webmagicx.conf.MatcherConverters._

@Deprecated
class WXRedisScheduler(pool: JedisPool, task: Task) extends RedisScheduler(pool) with Closeable{

  def this(sc:SpiderConf, task:Task, props:XmlProps) = this(
    new JedisPool(
      new GenericObjectPoolConfig(),
      props.valueOrDefault("host", "value")(strType)(Protocol.DEFAULT_HOST),
      props.valueOrDefault("port", "value")(intType)(Protocol.DEFAULT_PORT),
      props.valueOrDefault("timeout", "value")(intType)(Protocol.DEFAULT_TIMEOUT),
      props.valueOrNull("password", "value")(strType),
      props.valueOrDefault("database", "value")(intType)(Protocol.DEFAULT_DATABASE),
      props.valueOrNull("clientName", "value")(strType)
    )
    ,task
  )

  override def close(): Unit = {
    this.resetDuplicateCheck(task)
  }

}
