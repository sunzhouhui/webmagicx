package org.luosl.webmagicx.urlcreator

import org.luosl.webmagicx.conf.{XmlProps, SpiderConf}
import us.codecraft.webmagic.{Request, Task}

class StandardUrlCreator(sc:SpiderConf, task:Task, props:XmlProps) extends AbstractUrlCreator(sc, task, props) {

  override def requests(): Seq[Request] = Seq(new Request(url).setMethod(method))

}
