package org.luosl.webmagicx.listeners

import java.util.concurrent.atomic.AtomicInteger

import org.luosl.webmagicx.conf.Field
import play.api.libs.json.{JsObject, Json}
import us.codecraft.webmagic.{Page, ResultItems, Task}

/**
  * Created by luosl on 2017/12/15.
  */
class GeneralProcessorListener(clazz:Class[_]) extends ProcessorListener{

  val name:String = clazz.getSimpleName

  private val _successCount:AtomicInteger = new AtomicInteger(0)

  private val _errorCount:AtomicInteger = new AtomicInteger(0)

  private val _skipCount:AtomicInteger = new AtomicInteger(0)

  override def onSuccess(page: Page): Unit = _successCount.getAndIncrement()

  override def onError(page: Page): Unit = _errorCount.getAndIncrement()

  override def onSkip(page:Page, f:Field): Unit = _skipCount.getAndIncrement()

  def successCount:Long = _successCount.get()

  def errorCount:Long = _errorCount.get()

  def skipCount:Long = _skipCount.get()

}
