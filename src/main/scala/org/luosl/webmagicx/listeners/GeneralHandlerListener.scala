package org.luosl.webmagicx.listeners

import java.util.concurrent.atomic.AtomicInteger

import us.codecraft.webmagic.{ResultItems, Task}

class GeneralHandlerListener(clazz:Class[_]) extends HandlerListener {
  val name:String = clazz.getSimpleName

  private val _successCount:AtomicInteger = new AtomicInteger(0)

  private val _errorCount:AtomicInteger = new AtomicInteger(0)

  private val _skipCount:AtomicInteger = new AtomicInteger(0)

  override def onSuccess(resultItems: ResultItems, task: Task): Unit = _successCount.getAndIncrement()

  override def onError(resultItems: ResultItems, task: Task): Unit = _errorCount.getAndIncrement()

  override def onSkip(resultItems: ResultItems, task: Task): Unit = _skipCount.getAndIncrement()

  def successCount:Long = _successCount.get()

  def errorCount:Long = _errorCount.get()

  def skipCount:Long = _skipCount.get()

}
