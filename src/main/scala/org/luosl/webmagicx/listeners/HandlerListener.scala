package org.luosl.webmagicx.listeners

import us.codecraft.webmagic.{ResultItems, Task}

trait HandlerListener {

  def onSuccess(resultItems: ResultItems, task:Task)

  def onError(resultItems: ResultItems, task:Task)

  def onSkip(resultItems: ResultItems, task:Task)

}
