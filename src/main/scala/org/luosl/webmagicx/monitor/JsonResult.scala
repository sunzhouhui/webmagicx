package org.luosl.webmagicx.monitor

import play.api.libs.json.{JsObject, JsValue, Json}

class JsonResult(code:Int, msg:String, data:JsValue) {

  lazy val jsObj:JsObject = Json.obj(
    "code" -> code,
    "msg" -> msg,
    "data" -> data
  )

  lazy val jsStr:String = jsObj.toString()
}

object JsonResult{
  def apply(code: Int, msg: String, data: JsValue): JsonResult = new JsonResult(code, msg, data)
  def error(msg: String, data: JsValue): JsonResult = new JsonResult(500, msg, data)
  def error(data: JsValue): JsonResult = new JsonResult(500, "", data)
  def error(msg:String): JsonResult = error(msg, Json.obj())
  def success(msg: String, data: JsValue): JsonResult = new JsonResult(200, msg, data)
  def success(data: JsValue): JsonResult = new JsonResult(200, "", data)
  def success(msg: String): JsonResult = success(msg, Json.obj())
}
