package org.luosl.webmagicx.pipeline.component

import org.luosl.webmagicx.utils.Logging
import us.codecraft.webmagic.ResultItems

/**
  *
  * @param loadDistinctValueOp 从 ResultItems 获取去重字段的操作
  */
abstract class Distinct(loadDistinctValueOp:ResultItems => Any) extends Logging{
  /**
    * 添加一个 item 到去重器
    * @param item  item
    */
  def addItem(item: Any):Unit

  /**
    * 判断一个 item 是否为唯一
    * @param item item
    * @return
    */
  def isUnique(item:Any):Boolean

  /**
    * 判断一个 item 是否为唯一，当isUnique为true时 将调用addItem
    * @param items items
    * @return
    */
  def isUniqueAndAdd(items:ResultItems):Boolean = {
    val value:Any = loadDistinctValueOp(items)
    val unique:Boolean = isUnique(value)
    if(unique) {
      addItem(value)
    }
    unique
  }
}
