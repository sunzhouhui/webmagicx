package us.codecraft.webmagic.handler;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;

/**
 * Created by luosl on 2017/12/18.
 */
public interface Handler {
    void process(ResultItems items, Task task);
}
