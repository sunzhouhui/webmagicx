package us.codecraft.webmagic.utils;

import com.alibaba.fastjson.JSON;

/**
 * Created by luosl on 2018/3/16.
 */
public class JsonUtil {
    public static String toJSONString(Object object) {
        return JSON.toJSONString(object);
    }
}
